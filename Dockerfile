FROM python:3.9-alpine

COPY . .

RUN pip install -e .

CMD python3 -m toptica_exporter
