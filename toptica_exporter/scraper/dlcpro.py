import asyncio
import logging
import datetime
from contextlib import AsyncExitStack, asynccontextmanager
from typing import Any

from aioprometheus import Gauge
from aioprometheus.service import Service
from toptica.lasersdk.asyncio.dlcpro.v1_9_0 import DLCpro, NetworkConnection, Laser


log = logging.getLogger(__name__)

uptime = Gauge("toptica_uptime_seconds", "Laser system uptime")
interlock_open = Gauge("toptica_interlock_open", "Interlock status")
system_health = Gauge("toptica_system_health_mask", "System status bitmap")

system_info = Gauge("toptica_system_info", "Information about the system")
laser_info = Gauge("toptica_laser_info", "Information about the laser head")


emission = Gauge("toptica_emission_enabled", "Emission enabled")
laser_health = Gauge("toptica_laser_health_mask", "Laser heath bitmask")
laser_current_status = Gauge(
    "toptica_laser_current_status_mask", "Laser current controller status bitmask"
)
laser_ontime = Gauge("toptica_laser_ontime_seconds", "Total laser head ontime")
laser_current_set = Gauge(
    "toptica_laser_current_set_milliamperes", "Current setpoint including feed-forward"
)
laser_current_offset = Gauge(
    "toptica_laser_current_offset_milliamperes", "Current setpoint without feed-forward"
)
laser_current_act = Gauge("toptica_laser_current_milliamperes", "Actual laser current")
laser_current_clip = Gauge(
    "toptica_laser_current_clip_milliamperes", "Maximum allowed laser current"
)
laser_voltage_act = Gauge("toptica_laser_voltage_volts", "Actual laser voltage")
laser_voltage_clip = Gauge(
    "toptica_laser_voltage_clip_volts", "Maximum allowed laser voltage"
)
laser_temp_act = Gauge(
    "toptica_laser_temperature_celsius", "Actual laser diode temperature"
)
laser_temp_set = Gauge(
    "toptica_laser_temperature_setpoint_celsius", "Laser diode temperature setpoint"
)
laser_temp_current = Gauge(
    "toptica_laser_temperature_current_amperes", "Peltier current"
)
laser_temp_status = Gauge(
    "toptica_laser_temperature_status_mask", "Temperature controller status bitmask"
)
laser_piezo_voltage_act = Gauge(
    "toptica_laser_piezo_voltage_volts", "Actual piezo output voltage"
)
laser_piezo_voltage_set = Gauge(
    "toptica_laser_piezo_voltage_setpoint_volts", "Piezo output voltage setpoint"
)
laser_piezo_heatsink_temp = Gauge(
    "toptica_laser_piezo_heatsink_temperature_celsius", "Piezo heatsink temperature"
)
laser_piezo_status = Gauge(
    "toptica_laser_piezo_status_mask", "Laser piezo controller status bitmask"
)
laser_pressure_compensation_pressure = Gauge(
    "toptica_laser_pressure_compensation_air_pressure_hectopascal",
    "Air pressure measured by the pressure compensation",
)
laser_pressure_compensation_voltage = Gauge(
    "toptica_laser_pressure_compensation_voltage_volts",
    "Voltage difference due to pressure compensation",
)

relative_humidity = Gauge(
    "toptica_relative_humidity_percent",
    "Relative humidity measured by the main controller board",
)
laser_system_temp = Gauge(
    "toptica_system_temperature_celsius", "Temperatures measured by the controller box"
)

analog_in_voltage = Gauge("toptica_io_voltage", "Voltage at the analog input pin")
power_voltage = Gauge("toptica_supply_voltage_volts", "Internal system voltage")
power_current = Gauge(
    "toptica_supply_current_amperes", "Current consumption on internal supply rail"
)

msg_count = Gauge("toptica_messages", "Number of current laser messages")


def system_subscriptions(dlc: DLCpro):
    return (
        (dlc.uptime, uptime, {}),
        (dlc.interlock_open, interlock_open, {}),
        (dlc.system_health, system_health, {}),
        (dlc.system_messages.count, msg_count, {}),
        (dlc.mc.board_temp, laser_system_temp, {"object": "mc"}),
        (dlc.tc1.board_temp, laser_system_temp, {"object": "tc1"}),
        (dlc.power_supply.board_temp, laser_system_temp, {"object": "power-supply"}),
        (
            dlc.power_supply.heatsink_temp,
            laser_system_temp,
            {"object": "power-supply-heatsink"},
        ),
        (dlc.power_supply.current_5V, power_current, {"rail": "+5V"}),
        (dlc.power_supply.current_15V, power_current, {"rail": "+15V"}),
        (dlc.power_supply.current_15Vn, power_current, {"rail": "-15V"}),
        (dlc.power_supply.voltage_5V, power_voltage, {"rail": "+5V"}),
        (dlc.power_supply.voltage_15V, power_voltage, {"rail": "+15V"}),
        (dlc.power_supply.voltage_15Vn, power_voltage, {"rail": "-15V"}),
        (dlc.power_supply.voltage_3V3, power_voltage, {"rail": "+3.3V"}),
        (dlc.mc.relative_humidity, relative_humidity, {}),
        (dlc.io.fine_1.value_act, analog_in_voltage, {"input": 1}),
        (dlc.io.fine_2.value_act, analog_in_voltage, {"input": 2}),
        (dlc.io.fast_3.value_act, analog_in_voltage, {"input": 3}),
        (dlc.io.fast_4.value_act, analog_in_voltage, {"input": 4}),
    )


def laser_subscriptions(laser: Laser):
    return tuple(
        map(
            lambda x: (*x, {"laser": "laser1"}),
            (
                (laser.emission, emission),
                (laser.health, laser_health),
                (laser.dl.ontime, laser_ontime),
                (laser.dl.cc.current_act, laser_current_act),
                (laser.dl.cc.current_set, laser_current_set),
                (laser.dl.cc.current_clip, laser_current_clip),
                (laser.dl.cc.current_offset, laser_current_offset),
                (laser.dl.cc.voltage_act, laser_voltage_act),
                (laser.dl.cc.voltage_clip, laser_voltage_clip),
                (laser.dl.cc.status, laser_current_status),
                (laser.dl.tc.temp_act, laser_temp_act),
                (laser.dl.tc.temp_set, laser_temp_set),
                (laser.dl.tc.current_act, laser_temp_current),
                (laser.dl.tc.status, laser_temp_status),
                (laser.dl.pc.voltage_act, laser_piezo_voltage_act),
                (laser.dl.pc.voltage_set, laser_piezo_voltage_set),
                (laser.dl.pc.heatsink_temp, laser_piezo_heatsink_temp),
                (laser.dl.pc.status, laser_piezo_status),
                (
                    laser.dl.pressure_compensation.air_pressure,
                    laser_pressure_compensation_pressure,
                ),
                (
                    laser.dl.pressure_compensation.compensation_voltage,
                    laser_pressure_compensation_voltage,
                ),
            ),
        )
    )


class DLCproMonitoring:
    def __init__(self, host: str):
        self.host = host

    @asynccontextmanager
    async def _setup_subscription(self, param, metric, labels):
        labels["host"] = self.host

        def set_gauge_callback(
            time: datetime.datetime, topic: str, value: Any, error
        ) -> None:
            metric.set(labels, float(value))
            log.debug("%s %s: %s (%s; %s)", time, topic, value, labels, error)

        try:
            sub = await param.subscribe(set_gauge_callback)
            # set start value
            metric.set(labels, float(await param.get()))
        except Exception as e:
            log.critical("Failed to set up %s", param)
            log.exception(e)
            return

        try:
            yield sub
        finally:
            await sub.cancel()

    async def get_system_information(self, dlc: DLCpro):
        system_info.set(
            {
                "firmware_version": await dlc.fw_ver.get(),
                "software_version": await dlc.ssw_ver.get(),
                "serial_number": await dlc.serial_number.get(),
                "system_type": await dlc.system_type.get(),
                "system_model": await dlc.system_model.get(),
                "system_label": await dlc.system_label.get(),
                # "firmware_revision": await dlc.svn_revision.get(),
                # "software_revision": await dlc.ssw_svn_revision.get(),
                "build_number": await dlc.build_information.build_number.get(),
                "build_id": await dlc.build_information.build_id.get(),
                "build_tag": await dlc.build_information.build_tag.get(),
                "host": self.host,
            },
            1,
        )

    async def get_laser_information(self, laser: Laser):
        laser_info.set(
            {
                "type": await laser.dl.type_.get(),
                "product_name": await laser.product_name.get(),
                "version": await laser.dl.version.get(),
                "serial_number": await laser.dl.serial_number.get(),
                "model": await laser.dl.model.get(),
            },
            1,
        )

    async def monitor(self):
        async with DLCpro(NetworkConnection(self.host)) as dlc:
            await self.get_system_information(dlc)
            await self.get_laser_information(dlc.laser1)

            subscriptions = (
                *system_subscriptions(dlc),
                *laser_subscriptions(dlc.laser1),
            )
            async with AsyncExitStack() as stack:
                connections = [
                    await stack.enter_async_context(self._setup_subscription(*sub))
                    for sub in subscriptions
                ]
                log.info("%s: Subscribed to %s parameters", self.host, len(connections))

                # loop forever, using uptime as connection indicator.
                last_uptime = uptime.get(dict(host=self.host))
                while True:
                    await asyncio.sleep(60)
                    new_uptime = uptime.get(dict(host=self.host))
                    if new_uptime == last_uptime:
                        log.error("%s stopped updates, resetting", self.host)
                        # updates are paused, something went wrong
                        return
                    log.info("%s connected, uptime %s", self.host, new_uptime)
                    last_uptime = new_uptime

    async def run(self):
        while True:
            try:
                await self.monitor()
            except Exception as e:
                log.exception(e)
            log.critical("monitor crashed for host %s, restarting after 10s", self.host)
            await asyncio.sleep(10)


async def main(*hosts):
    lasers = [DLCproMonitoring(host) for host in hosts]
    svr = Service()
    try:
        await svr.start(port=8080)
        await asyncio.gather(*(laser.run() for laser in lasers))
    finally:
        await svr.stop()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    asyncio.run(main("dlcpro-blue.cslab", "dlcpro-master.cslab", "dlcpro-repump.cslab"))
