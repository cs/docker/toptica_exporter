import asyncio
import logging

from aioprometheus.service import Service

from .scraper.dlcpro import DLCproMonitoring


async def main(*devices):
    svr = Service()
    try:
        await svr.start(port=8080)
        await asyncio.gather(*(dev.run() for dev in devices))
    finally:
        await svr.stop()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    asyncio.run(
        main(
            DLCproMonitoring("dlcpro-blue.cslab"),
            DLCproMonitoring("dlcpro-master.cslab"),
            DLCproMonitoring("dlcpro-repump.cslab"),
        )
    )
